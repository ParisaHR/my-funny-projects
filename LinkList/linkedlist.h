#ifndef LINKEDLIST_H
#define LINKEDLIST_H

typedef struct node
{
  char  data;
  node *next;
} *nodePtr;

class LinkedList
{
public:
  LinkedList();

  void     AddData(char addData);

  void     RemoveData(char delData);

  void     PrintData();

  void     PrintReverseData(nodePtr start);

  int      count(nodePtr start);

  void     printUpperLowercase();

  void     Search(char &Item);

  nodePtr  getHead() const;

private:
  nodePtr  head;
  nodePtr  curr;
  nodePtr  temp;
};

#endif // LINKEDLIST_H
