#include "linkedlist.h"
#include <iostream>
using namespace std;

LinkedList::LinkedList()
{
  head = nullptr;
  curr = nullptr;
  temp = nullptr;
}

void  LinkedList::AddData(char addData)
{
  nodePtr  n = new node;

  n->next = nullptr;
  n->data = addData;

  if (head != nullptr)
  {
    curr = head;

    while (curr->next != nullptr)
    {
      curr = curr->next;
    }

    curr->next = n;
  }
  else
  {
    head = n;
  }
}

void  LinkedList::RemoveData(char delData)
{
  nodePtr  delPtr = nullptr;

  temp = head;
  curr = head;

  while (curr != nullptr && curr->data != delData)
  {
    temp = curr;
    curr = curr->next;
  }

  if (curr == nullptr)
  {
    cout << delData << " Was not in the list " << endl;
    delete delPtr;
  }
  else
  {
    delPtr     = curr;
    curr       = curr->next;
    temp->next = curr;

    if (delPtr == head)
    {
      head = head->next;
      temp = nullptr;
    }

    delete delPtr;
    cout << "The Value" << delData << "was deleted" << endl;
  }

  PrintData();
}

void  LinkedList::PrintData()
{
  curr = head;

  while (curr != nullptr)
  {
    cout << curr->data;
    curr = curr->next;
  }
}

void  LinkedList::PrintReverseData(nodePtr start)
{
  if (!start)
  {
    return;
  }

  PrintReverseData(start->next);
  cout << start->data;
}

int  LinkedList::count(nodePtr start)
{
  if (start == nullptr)
  {
    return 0;
  }

  return 1 + count(start->next);
}

void  LinkedList::printUpperLowercase()
{
  curr = head;

  while (curr != nullptr)
  {
    if ((curr->data >= 97) && (curr->data <= 122))
    {
      curr->data = curr->data - 32;
      cout << curr->data;
      curr = curr->next;
    }
    else if ((curr->data >= 65) && (curr->data <= 92))
    {
      curr->data = curr->data + 32;
      cout << curr->data;
      curr = curr->next;
    }
    else
    {
      cout << curr->data;
      curr = curr->next;
    }
  }
}

void  LinkedList::Search(char &Item)
{
  curr = head;

  while (curr != nullptr)
  {
    if (curr->data == Item)
    {
      cout << "Found " << curr->data;

      return;
    }
    else
    {
      if (curr->next == nullptr)
      {
        cout << "not Found" << endl;
      }

      curr = curr->next;
    }
  }
}

nodePtr  LinkedList::getHead() const
{
  return head;
}
