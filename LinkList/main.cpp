#include <iostream>
#include <string>
#include "linkedlist.h"
using namespace std;

int  showMenu(LinkedList *list)
{
  int  num = 0;

  cout << endl;
  cout << "Choose one of this options : " << endl;
  cout << "1 . show String Length :  " << endl;
  cout << "2 . Find Specific char : " << endl;
  cout << "3 . Print String  " << endl;
  cout << "4 . Print Revers of String" << endl;
  cout << "5 . show Upper/Lower case  " << endl;
  cout << "6 . Remove char  " << endl;
  cout << "7 . Exit ApplicatishowMenuon  " << endl;
  cout << "your choose is =   ";
  cin >> num;

  if ((num >= 1) && (num <= 7))
  {
    switch (num)
    {
    case 1:
      cout << "String Length is " << list->count(list->getHead()) << endl;
      break;
    case 2:
    {
      char  a;
      cout << " enter character :";
      cin >> a;
      list->Search(a);
    }
    break;
    case 3:
      list->PrintData();
      break;
    case 4:
      list->PrintReverseData(list->getHead());
      break;
    case 5:
      list->printUpperLowercase();

      break;
    case 6:
      char  a;
      cout << " enter character :";
      cin >> a;
      list->RemoveData(a);
      break;
    }
  }
  else
  {
    cout << "invalid input ";
  }

  return num;
}

int  main()
{
  cout << "Please Enter Your String : " << endl;
  string  data;
  getline(cin, data);

  LinkedList *_list = new LinkedList;

  for (std::string::iterator it = data.begin(); it != data.end(); ++it)
  {
    _list->AddData(*it);
  }

  int  a = 0;

  while (a != 7)
  {
    a = showMenu(_list);
  }

  return 0;
}
